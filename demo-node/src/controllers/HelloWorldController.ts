import {Request, Response} from "express";

class HelloWorldController {

    public async get(req: Request, res: Response) {
        res.send('Hello World')
    }
}

export default HelloWorldController