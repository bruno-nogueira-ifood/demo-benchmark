import { Router } from 'express'
import HelloWorldController from "../controllers/HelloWorldController"

const router = Router()
const helloWorldController = new HelloWorldController()

router.get('/', helloWorldController.get)

export default router