import express, { Application, Router } from 'express'
import helloWorldRouter from './routers/HelloWorldRouters'

class Server {
    private app

    constructor() {
        this.app = express()
        this.routerConfig()
    }

    private routerConfig() {
        this.app.use('/', helloWorldRouter)
    }

    public start = (port: number) => {
        return new Promise(((resolve, reject) => {
            this.app.listen(port, () => {
                resolve(port)
            }).on('error', (err: Object) => reject(err))
        }))
    }

}

export default Server