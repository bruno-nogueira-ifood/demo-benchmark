## Dependências
* JDK 11 (minimo)
* Docker
* docker-compose

## Como executar o projeto?

O projeto está dividido em 5 diretórios, cada um executando de maneira independente. O projeto faz uso da ferramenta Make, o que descreve as construções dos projetos e imagens se você estiver usando o ambiente Linux. Então basta que você entre em cada diretório (exceto o diretório `demo-loadtest`) e simplesmente digite `make` na linha de comando que o projeto será devidamente construído e uma imagem docker será gerada.

Para executar o load-test basta entrar no diretório `demo-loadtest` e executar o comando `make`, porém especificando qual o projeto com LT você deseja executar:

* Spring WebMVC
    ```shell script
    make load-test-mvc 
    ```
* Spring WebFlux
    ```shell script
     make load-test-reactive
    ```
  
* Quarkus
    ```shell script
     make load-test-quarkus
    ```  
  
* Micronaut
    ```shell script
     make load-test-micronaut
    ``` 
