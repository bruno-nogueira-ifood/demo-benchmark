package com.example.demowebmvc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoWebmvcApplication

fun main(args: Array<String>) {
    runApplication<DemoWebmvcApplication>(*args)
}
