package com.example.demoreactive

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoReactiveApplication

fun main(args: Array<String>) {
    runApplication<DemoReactiveApplication>(*args)
}
