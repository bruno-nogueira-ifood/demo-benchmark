package simulations.scenarios

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class GetHelloWorld {

  def getHelloWorld: ChainBuilder = {
    exec(http("Say Hello World")
      .get("/")
      .check(status.is(200)))
  }

}
