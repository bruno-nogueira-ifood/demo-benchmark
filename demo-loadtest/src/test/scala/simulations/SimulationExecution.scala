package simulations

import io.gatling.core.Predef._
import simulations.scenarios.GetHelloWorld

import scala.concurrent.duration.DurationInt

class SimulationExecution extends GenericSimulation {

  private val scn = scenario("Hello World Load Test")
    .forever() {
      exec(new GetHelloWorld().getHelloWorld)
    }

  setUp(
    scn.inject(
      rampUsers(userCount) during (rampDuration.second)
    ).protocols(httpConf.inferHtmlResources())
  ).maxDuration(testDuration.second)
}
